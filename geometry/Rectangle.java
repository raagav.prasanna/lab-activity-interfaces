package geometry;
public class Rectangle implements Shape{
    public double length;
    public double width;
    public Rectangle(double length, double width) {
        this.width = width;
        this.length = length;
    }
    public double getLength() {
        return this.length;
    }
    public double getWidth() {
        return this.width;
    }
    public double getPerimeter() {
        return (2 * (this.length + this.width));
    }
    public double getArea() {
        return (this.length * this.width);
    }
}