package geometry;
public class LotsOfShapes {
    public static void main(String[] args) {
        Shape[] shapes_arr = new Shape[5];
        shapes_arr[0] = new Rectangle(7,5);
        shapes_arr[1] = new Rectangle(3,4);
        shapes_arr[2] = new Circle(5);
        shapes_arr[3] = new Circle(2);
        shapes_arr[4] = new Square(7);

        for (Shape my_shape : shapes_arr) {
            System.out.println(my_shape.getClass().getName());
            System.out.println(my_shape.getArea());
            System.out.println(my_shape.getPerimeter());
            System.out.println("______________________");
        }  
    }
}